from django.urls import path
from .views import PostList, PostDetail, UserDetail, UserList, CategoryDetail, CategoryList, ApiRoot
from rest_framework.urlpatterns import format_suffix_patterns
# from firstapp import views ,UPER VALI LINE NU EDA V LIKH SAKDE A ASI ede lyi fr niche eda likhna pena path('', views.post_list, name='list'),


urlpatterns = [
    path('', ApiRoot.as_view(), name='root'),
    path('categories/', CategoryList.as_view(), name='categories'),
    path('categories/<int:pk>/', CategoryDetail.as_view(), name='single_category'),
    path('users/', UserList.as_view(), name='users'),
    path('users/<int:pk>/', UserDetail.as_view(), name='single-user'),
    path('posts/', PostList.as_view(), name='posts'),
    path('posts/<int:pk>/', PostDetail.as_view(),
         name='single-post'),  # pk de jga id v likh SAKDE
]
urlpatterns = format_suffix_patterns(urlpatterns)

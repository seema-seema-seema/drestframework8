from .models import Post, Category
from .serializers import PostSerializer, CategorySerializer, UserSerializer
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework import permissions
from firstapp.permissions import IsOwnerOrReadOnly
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.views import APIView

# Create your views here.


class PostList(generics.ListCreateAPIView):
    # NOTE : serializer_class  &  queryset  VARIABLE NAMES PREdefined h in listcreateAPIview de vich ,ena de jga horn name ni le sakde apa
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]

    queryset = Post.objects.all()
    serializer_class = PostSerializer


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class CategoryList(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryDetail(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ApiRoot(APIView):
    def get(self, request, format=None):
        return Response({
            # eh krn toh pehla asi url.py ch dena +category detail vle uper 2 functions jada bnaye ede ch
            # users ,posts,categiories vgera yrls,py ch name= vale same name use kite
            'users': reverse('users', request=request, format=format),
            'posts': reverse('posts', request=request, format=format),
            'categories': reverse('categories', request=request, format=format),

        })
